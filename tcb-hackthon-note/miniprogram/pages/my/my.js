// pages/my/my.js
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    count:0,
    level:0,
    nickName:'',
    _openid:'',
    nickNameFix:''

  },
  getInputValue(e){
    console.log(e.detail)// {value: "ff", cursor: 2}  
    this.setData({
      nickNameFix:e.detail.value
    })
  },
  level() {
    var that=this
    wx.navigateTo({
      url: '../level/level?count='+that.data.count
    })
  },
  welfare() {
    var that=this
    wx.navigateTo({
      url: '../welfare/welfare'
    })
  },
  updateName(){
    var that=this
    if(that.data.nickNameFix==that.data.nickName){
      return
    }
    if(that.data.nickNameFix.length>10){
      wx.showToast({
        icon: 'none',
        title: '长度不超过10'
      })
      return
    }
 
      db.collection('ranking').where({
        _openid: that.data._openid,
      }).update({ //更新记录ranking
        data: {
          nickName:that.data.nickNameFix
        }
      }).then(res => {  
        wx.showToast({
          icon: 'none',
          title: '修改成功'
        })
      }).catch(console.error)
      
   
  },
  getList:function(){
    var that=this
    //1、查询状态为是否存在打卡记录
    wx.cloud.callFunction({
      name: 'getopenid',//调用云函数获取用户唯一openid
      complete: res => {
        const openid = res.result.openid
        db.collection('ranking').where({
          _openid: openid,
        })
        .get({
          success: function(res) {
            if(res.data.length>0){
             
              var count=res.data[0].count
              var level=0
              if(count<=21){
                level=0
              }
              if(count>21&&count<=90){
                level=1
              }
              if(count>90&&count<=270){
                level=2
              }
              if(count>270&&count<=540){
                level=3
              }
              if(count>540&&count<=900){
                level=4
              }
              if(count>900&&count<=1350){
                level=5
              }
              if(count>1350&&count<=1890){
                level=6
              }
              if(count>1890){
                level=7
              }
              that.setData({
                count:count,
                level:level,
                nickName:res.data[0].nickName,
                nickNameFix:res.data[0].nickName,
                _openid:openid
              })
           
            }
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})