// miniprogram/pages/level/level.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[
      {color:"background_green",level:"LV 7",content:"王者肛(>1890)"},
    {color:"background_green",level:"LV 6",content:"钻石肛（1351-1890）"},
    {color:"background_green",level:"LV 5",content:"铂金肛（901-1350）"},
    {color:"background_green",level:"LV 4",content:"黄金肛（541-900）"},
    {color:"background_green",level:"LV 3",content:"白银肛（271-540）"},
    {color:"background_green",level:"LV 2",content:"青铜肛（91-270）"},
    {color:"background_green",level:"LV 1",content:"黑铁肛（22-90）"},
    {color:"background_green",level:"LV 0",content:"菜鸟肛（0-21）"}
  ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    var count=options.count
    if(count<=21){
      that.setData({
        'list[7].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>21&&count<=90){
      that.setData({
        'list[6].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>90&&count<=270){
      that.setData({
        'list[5].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>270&&count<=540){
      that.setData({
        'list[4].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>540&&count<=900){
      that.setData({
        'list[3].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>900&&count<=1350){
      that.setData({
        'list[2].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>1350&&count<=1890){
      that.setData({
        'list[1].color':'background_red',   //数据路径key必须带''号
      })
    }
    if(count>1890){
      that.setData({
        'list[0].color':'background_red',   //数据路径key必须带''号
      })
    }

	       
       
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})