const util = require('../../utils/util.js');
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    diary:[],
 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    
  },

  
  //云函数获取diary集合数据
  getTask: function () {
 
   
    wx.cloud.callFunction({
      // 云函数名称
      name: 'getRanking',
      // 传给云函数的参数
      data: {
     
      }
    }).then(res => {
      let data = res.result.data;
      console.log(data)
      this.setData({
        diary: data
      })

    }).catch(console.error)
  },



  onPullDownRefresh: function () {
    this.getTask()
    wx.stopPullDownRefresh()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   this.getTask()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})