const server = "http://localhost:8443/"


module.exports = {
    loginUrl: server + 'wx/api/login', 
    HabitUrl: server + 'habit',
    ClockInUrl: server +'clockin_record',
    IconCategory: server + 'admin/icon_category',
    ClockInLogUrl: server + 'clockin_Log',
    TomatoUrl:server + 'tomato'
  };